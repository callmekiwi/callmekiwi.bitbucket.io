/*
------------------------------------------
	site wide CIRCULAR NAVIGATION
------------------------------------------
*/
toggle = document.getElementById("toggle");
nav = document.getElementById("mainNav");
toggle_open_text = "menu";
toggle_close_text = "close";

toggle.addEventListener(
	"click",
	function() {
		nav.classList.toggle("open");

		if (nav.classList.contains("open")) {
			toggle.innerHTML = toggle_close_text;
		} else {
			toggle.innerHTML = toggle_open_text;
		}
	},
	false
);

setTimeout(function() {
	nav.classList.toggle("open");
}, 600);

/*
------------------------------------------
	MACY -- masonry layout for portfolio
------------------------------------------
*/
var masonry = new Macy({
	container: '#masonry-gallery',
	trueOrder: false,
	waitForImages: false,
	useOwnImageLoader: false,
	debug: true,
	mobileFirst: true,
	columns: 1,
	margin: 8,
	breakAt: {
		1200: 6,
		992:  5,
		768:  4,
		500:  3,
		320:  2
	}
});

/*
------------------------------------------
	BAGUETTE_BOX -- lightbox for portfolio
------------------------------------------
*/
baguetteBox.run('#masonry-gallery');
