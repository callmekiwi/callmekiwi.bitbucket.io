toggler = document.getElementById("mainNavToggler");
nav = document.getElementById("mainNav");
toggler_open_text_html = "<span>MENU</span>";
toggler_close_text_html = "<span>CLOSE</span>";

toggler.addEventListener(
	"click",
	function() {
		nav.classList.toggle("open");
		if (nav.classList.contains("open")) {
			toggler.innerHTML = toggler_close_text_html;
		} else {
			toggler.innerHTML = toggler_open_text_html;
		};
	},
	false
);

setTimeout(function() {
	toggler.click();
}, 600);
